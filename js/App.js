var app = (function() {

    var api = {
        views: {},
        models: {},
        collections: {},
        content: null,
        router: null,
        note:null,
        notes: null,
        user: null,
        users: null,
        element:null,
        init: function() {
            ViewsFactory.menu();
            this.user = new api.models.user;
            this.users = new api.collections.users; 
            this.note = new api.models.note;
            this.notes = new api.collections.notes;             
            Backbone.history.start();
            return this;
        }
    };

    var ViewsFactory = {
        menu: function() {
            if(!this.menuView){
                this.menuView = new api.views.menu({
                    el: $("#menu")
                });
            }            
            return this.menuView;
        },
        login: function() {
            if(!this.loginView) {
                this.loginView = new api.views.login({
                    model: api.user,
                    collection: api.users,                    
                    el: $('#work')
                });
            }
            return this.loginView;
        },
        registration: function() {
            if(!this.registrationView) {
                this.registrationView = new api.views.registration({
                    model: api.user,
                    collection: api.users,
                    el: $('#work')
                });
            }
            return this.registrationView;
        },
        information: function() {
            if(!this.informationView) {
                this.informationView = new api.views.information({
                    el: $('#work')
                });
            }
            return this.informationView;
        },
        main: function() {
            if(!this.mainView) {
                this.mainView = new api.views.main({
                    el: $('#work')
                });
            }
            new api.views.menu();           
            return this.mainView;
        },
        form: function() {
            if(!this.formView) {
                this.formView = new api.views.form({
                    el: $('#shit'),
                    collection: api.notes,
                    model: api.note,
                });
            }         
            return this.formView;
        },
        list: function() {
            if(!this.listView) {
                this.listView = new api.views.list({
                    el: $('#column'),
                    collection: api.notes,
                    model: api.note,                    
                });
            }         
            return this.listView;
        }          
    };

    var Router = Backbone.Router.extend({
        routes: {
            "": "index",
            "login" : "login",
            "registration" : "registration",
            "information" : "information",
            "main" : "main"
        },
        index: function() {
            console.log('index action');
        },
        login:function(){
            console.log('login action');
            var view = ViewsFactory.login();
            view.render();
        },
        registration:function(){
            console.log('registration action');
            var view = ViewsFactory.registration();
            view.render();
        },
        main:function(){
            console.log('main action');
            var view = ViewsFactory.main();
            view.render();    
            this.form();
            this.list();
        },
        form:function(){
            console.log('form action');
            var view = ViewsFactory.form();
            view.render(); 
        },
        list:function(){
            console.log('list action');
            var view = ViewsFactory.list();
            view.render(); 
        },        
        information:function(){
            console.log('information action');
            var view = ViewsFactory.information();
            view.render();
        }        

    });

    api.router = new Router();

    return api;

})();