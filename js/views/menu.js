app.views.menu = Backbone.View.extend({
    template: function(){
        if(!this.authSession){
            this.authSession = $("#authenticationSession").val();
            if(this.authSession.length > 0){
                return _.template($("#authorized-menu").html());
            }else{
                return _.template($("#unauthorized-menu").html());
            }
        }
    },
    initialize: function() {
        this.render();
    },
    render: function(){
        el = $("#menu");
        this.$el.html(this.template());
    }
});