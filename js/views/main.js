app.views.main = Backbone.View.extend({
    spanClass:'transporant',
    message:'',    
    events: {
        'click button': 'submit'
    },
    initialize: function() {
        this.listenTo(this.collection, 'successOnFetch', this.handleSuccess);
        this.listenTo(this.collection, 'errorOnFetch', this.handleError);
    },
    render: function() {
        var template, html, vars;
        template = _.template($("#main").html());    
        html = template(vars);
        this.$el.html(html);
        
        this.delegateEvents();
        return this;
    },
    submit: function(e) {
        e.preventDefault();
    
    },
    handleSuccess: function (response) {
        var message = response.message; 
        app.router.navigate('list', true);
    },

    handleError: function (response) {
        this.message = response.responseJSON.message;
        this.spanClass = 'error';
        this.render();        
    }      
});