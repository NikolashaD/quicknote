app.views.list = Backbone.View.extend({
    events: {
        'click a[data-delete]': 'delete',
        'click a[data-favorite]': 'favorite',
    },
    initialize: function() {     
        //var handler = _.bind(this.render, this);
        this.listenTo(this.collection, 'successOnFetch', this.handleSuccess);
        this.listenTo(this.collection, 'errorOnFetch', this.handleError);        
    },
    render: function() {
        this.collection.getNotes();
    },
    delete: function(e) {
        var noteId = parseInt(e.target.parentNode.getAttribute("data-id"));
        this.collection.delete(noteId);
    },
    favorite: function(e) {

    },
    handleSuccess: function (response) {
        if(typeof response.notes != 'undefined'){
            var message = response.message;
            var notes = response.notes;  
            var html = '';
            if(notes){
                $.each(notes, function(ind, val) {            
                    var template = _.template($("#list").html());    
                    html += '<div class="project">';
                    html += template({
                        id: val.id,
                        title: val.title,
                        created: val.date,
                        content: val.content,
                        isFavorite: false
                    });  
                    html += '</div>';
                }); 
            }

            this.$el.html(html);
            this.delegateEvents();      
            return this;
        }else{
            this.render();
        }
    },

    handleError: function (response) {
        this.message = response.responseJSON.message; 
        app.router.navigate('main', true);    
    }       

});