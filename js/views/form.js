app.views.form = Backbone.View.extend({
    index: false,
    events: {
        'click button[data-save]': 'submit'
    },
    initialize: function() {
        this.listenTo(this.collection, 'successOnFetch', this.handleSuccess);
        this.listenTo(this.collection, 'errorOnFetch', this.handleError);
    },
    render: function() {
        var template, html, vars;
        template = _.template($("#form").html());    
        html = template(vars);
        this.$el.html(html);
        
        this.delegateEvents();
        return this;
    },
    submit: function(e) {
        e.preventDefault();
        var title = this.$el.find("#title").val();
        var content = this.$el.find("#content").val();
        
        this.collection.save({
            title: title,
            content: content
        });   
    },
    handleSuccess: function (response) {
        var message = response.message;
        app.router.navigate('main', true);    
    },

    handleError: function (response) {
        this.message = response.responseJSON.message; 
        app.router.navigate('main', true);    
    }      
});