app.views.registration = Backbone.View.extend({
    spanClass:'transporant',
    message:'',
    events: {
        'click button[data-registration]': 'submit'
    },
    initialize: function() {
        this.listenTo(this.collection, 'successOnFetch', this.handleSuccess);
        this.listenTo(this.collection, 'errorOnFetch', this.handleError);        
        //this.render();
    },
    render: function() {
        var template, html, vars;
        template = _.template($("#registration").html());
        vars = {
            spanClass:this.spanClass,
            message:this.message
        };     
        html = template(vars);
        this.$el.html(html);
        
        this.delegateEvents();
        return this;
    },
    submit: function(e) {
        e.preventDefault();
        var firstName = this.$el.find("#firstName").val();
        var lastName = this.$el.find("#lastName").val();
        var userName = this.$el.find("#userName").val();
        var password = this.$el.find("#password").val();
        var email = this.$el.find("#email").val();

        this.collection.registration({
            firstName: firstName,
            lastName: lastName,
            userName: userName,
            password: password,
            email: email
        });
    },
    handleSuccess: function (response) {
        var message = response.message; 
        
//        var informationView = new app.views.information();
//        informationView.spanClass = 'success';
//        informationView.message = message;
//        informationView.render();
        
//        this.informationView = new app.views.information({
//            spanClass: 'success',
//            message:message
//        });
            
//        return this.informationView;        
        app.router.navigate('information', {trigger:true});
    },

    handleError: function (response) {
        this.message = response.responseJSON.message;
        this.spanClass = 'error';
        this.render();        
    }    
});