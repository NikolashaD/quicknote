app.views.login = Backbone.View.extend({
    spanClass:'transporant',
    message:'',    
    events: {
        'click button[data-login]': 'submit'
    },
    initialize: function() {
        this.listenTo(this.collection, 'successOnFetch', this.handleSuccess);
        this.listenTo(this.collection, 'errorOnFetch', this.handleError);
    },
    render: function() {
        var template, html, vars;
        template = _.template($("#login").html());
        vars = {
            spanClass:this.spanClass,
            message:this.message
        };     
        html = template(vars);
        this.$el.html(html);
        
        this.delegateEvents();
        return this;
    },
    submit: function(e) {
        e.preventDefault();
        var userName = this.$el.find("#userName").val();
        var password = this.$el.find("#password").val();
        
        this.collection.login({
            userName: userName,
            password: password
        });        
    },
    handleSuccess: function (response) {
        var message = response.message;
        var authToken = $("#authenticationSession").val(response.data.accessToken); 
        app.router.navigate('main', true);
    },

    handleError: function (response) {
        this.message = response.responseJSON.message;
        this.spanClass = 'error';
        this.render();        
    }      
});