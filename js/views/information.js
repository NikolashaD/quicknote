app.views.information = Backbone.View.extend({
    spanClass:'success',
    message:'Congratulations! Now you are registered. Try to log in.',    
    events: {
    },
    initialize: function(){
        this.render();
    },
    render: function() {            
        var template, html, vars;
        template = _.template($("#information").html());
        vars = {
            spanClass:this.spanClass,
            message:this.message
        };
        html = template(vars);
        this.$el.html(html);            

        return this;
    },
    submit: function(e) {
    }
    
});