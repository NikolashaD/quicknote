app.collections.notes = Backbone.Collection.extend({
    initialize: function(){
    },    
    save: function(data){
        var self = this;
        
        this.fetch({
            url:'http://localhost/quickNoteAPI/api/note/create',
            data:data,
            type:'POST',
            reset: true,
            success: function (collection, response, options) {                
                self.trigger('successOnFetch', response);
            },
            error: function (collection, response, options) {
                self.trigger('errorOnFetch', response);
            }            
        });        
    }, 
    delete: function(noteId){
        var self = this;
        
        this.fetch({
            url:'http://localhost/quickNoteAPI/api/note/delete/'+noteId,      
            type:'GET',
            reset: true,
            success: function (collection, response, options) {                
                self.trigger('successOnFetch', response);
            },
            error: function (collection, response, options) {
                self.trigger('errorOnFetch', response);
            }            
        });        
    },     
    getNotes: function(){
        var self = this;
        
        this.fetch({
            url:'http://localhost/quickNoteAPI/api/notes',
            type:'GET',
            reset: true,
            success: function (collection, response, options) {                
                self.trigger('successOnFetch', response);
            },
            error: function (collection, response, options) {
                self.trigger('errorOnFetch', response);
            }            
        });        
    },     
    model: app.models.note
});