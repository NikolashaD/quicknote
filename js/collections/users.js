app.collections.users = Backbone.Collection.extend({   
    initialize: function(){
    },    
    
    registration: function(data){
        var self = this;
        
        this.fetch({
            url: 'http://localhost/quickNoteAPI/api/registration',
            data:data,
            type:'POST',
            reset: true,
            success: function (collection, response, options) {                
                self.trigger('successOnFetch', response);
            },
            error: function (collection, response, options) {
                self.trigger('errorOnFetch', response);
            }            
        });        
    },
    
    login: function(data){
        var self = this;
        
        this.fetch({
            url:'http://localhost/quickNoteAPI/api/login',
            data:data,
            type:'POST',
            reset: true,
            success: function (collection, response, options) {                
                self.trigger('successOnFetch', response);
            },
            error: function (collection, response, options) {
                self.trigger('errorOnFetch', response);
            }            
        });        
    },    

    
    collection: app.collections.users
});