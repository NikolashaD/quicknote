app.models.user = Backbone.Model.extend({    
    defaults: {
        firstName: '',
        lastName: '',
        userName: '',
        password: '',
        email: ''
    },   
    model: app.models.user
});